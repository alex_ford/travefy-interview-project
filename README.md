Travefy Interview Project README
================================

Interview project for Travefy. See [Project Details](./docs/project-details.md) for the original description provided for this project.

This leverages the following technologies:

* Ember.js
    * [Website](https://emberjs.com/)
    * [Guide Docs](https://guides.emberjs.com/release/)
    * [API Docs](https://api.emberjs.com/ember/release)
    * [CLI Docs](https://cli.emberjs.com/release/)
* Ember Paper
    * [Github](https://github.com/miguelcobain/ember-paper))
    * [Docs](https://miguelcobain.github.io/ember-paper/)
* Liquid Fire
    * [Docs](https://ember-animation.github.io/liquid-fire/)
    * [on Ember Observer](https://www.emberobserver.com/addons/liquid-fire)
* Material Design
    * [Website](https://material.io/)
    * [Icon Browser](https://material.io/tools/icons/?icon=check_box_outline_blank&style=baseline)
* Ember Moment (Moment.js)
    * [Github](https://github.com/stefanpenner/ember-moment)
    * [Moment.js Website](http://momentjs.com/)

# Prerequisites

You will need the following things properly installed on your computer in order to use this project:

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
    * Use the `./scripts/_install-nodejs.sh` if needed/desired.
* [Ember CLI](https://ember-cli.com/)
    * Install with the `./scripts/install-dev.sh` script if needed/desired.
* [Google Chrome](https://google.com/chrome/)

# Installation

```bash
git clone git@gitlab.com:alex_ford/travefy-interview-project.git
cd travefy-interview-project/

# Optional, run these if you need them
./scripts/_install-nodejs.sh
./scripts/_install-watchman.sh

# Setup NodeJS development environment for this project
./scripts/install-dev.sh
npm install

# Extract (you'll need the password), or create, your secrets for API access
./scripts/restore-secrets.sh
```

# Secrets

* Secret, sensitive data and information (e.g. API private keys, etc.) must be provided in the `./secrets/` directory before running the app. Failure to specify the required secrets will result in the app not working!
* Use the `./scripts/save-secrets.sh` and `./scripts/restore-secrets.sh` scripts accordingly.
* To use the secrets contained within the `./secrets.tgz.pgp` file, you will need a separate password.

For more information, see the [Secrets](./docs/secrets.md) doc.

# Running

Using two terminals, execute the following:

Terminal 1
```bash
./scripts/run-ember-server.sh
```

Terminal 2
```bash
./scripts/run-google-chrome.sh
```

The scripts above include slight configuration tweaks in order to make it easier to launch the Ember Server and an instance of Google Chrome pointed at the webapp's URL (http://localhost:4000).

You can also see an HTML Report of the tests at [http://localhost:4200/tests](http://localhost:4200/tests).

# Development

[Microsoft VS Code](https://code.visualstudio.com/) workspace/project files are provided. The start script which launches VS Code will do so in an "independent" working environment. (The User Data and Extensions directories have been changed from their system defaults.)

After installing VS Code on your system, launch VS Code with the following:

```bash
./start-vscode.sh
```

Then proceed to install all recommended extensions.

# Testing

Testing is done via ember's basic testing mechanisms... but a script has been included anyway:

```bash
./scripts/test.sh
```

# API Test Scripts and Sample Data

Under the `./scripts/api/` directory there are several bash scripts which exercise the Travefy API from the Bash command-line. These can be helpful in lieu of something more powerful (such as [Postman](https://www.getpostman.com/)).

Under the `./sample-data/` directory, there are several `.json` files, which are sanitized responses received from the Travefy API.

# Deployment

TO BE DETERMINED...
