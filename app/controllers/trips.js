import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    goToTripDetails(tripId) {
      this.transitionToRoute("trip-details", { queryParams: { tripId: tripId } });
    }
  }
});
