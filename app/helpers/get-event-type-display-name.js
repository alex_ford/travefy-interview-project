import { helper } from '@ember/component/helper';

import EventTypes from '../models/event-types';

export function getEventTypeDisplayName([eventTypeId]) {
  if (eventTypeId == null) {
    return EventTypes[-1].DisplayName;
  } else {
    return EventTypes[eventTypeId].DisplayName;
  }
}

export default helper(getEventTypeDisplayName);
