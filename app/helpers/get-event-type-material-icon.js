import { helper } from '@ember/component/helper';

import EventTypes from '../models/event-types';

export function getEventTypeMaterialIcon([eventTypeId]) {
  if (eventTypeId == null) {
    return EventTypes[-1].MaterialIcon;
  } else {
    return EventTypes[eventTypeId].MaterialIcon;
  }
}

export default helper(getEventTypeMaterialIcon);
