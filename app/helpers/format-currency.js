import { helper } from '@ember/component/helper';

/**
 * Takes a float value and formats it properly as a currency string.
 *  e.g.  2.5 --> "$2.50"
 *        123.4567 --> "$123.46"
 *        541 --> "$541.00"
 * @param {*} param0
 */
export function formatCurrency([value]) {
  let currencyNumber = Number(value);
  return "$" + currencyNumber.toFixed(2);
}

export default helper(formatCurrency);
