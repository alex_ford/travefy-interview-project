import DS from 'ember-data';
import EventTypes from './event-types';

export default DS.Model.extend({
  Id: DS.attr("number"),
  TripDayId: DS.attr("number"),
  IsActive: DS.attr("boolean"),
  SegmentProviderName: DS.attr(),
  SegmentProviderPhone: DS.attr(),
  SegmentProviderUrl: DS.attr(),
  SegmentIdentifier: DS.attr(),
  Ordinal: DS.attr("number"),
  Name: DS.attr("string"),
  Description: DS.attr("string"),
  StartTimeZoneId: DS.attr(),
  StartTimeInMinutes: DS.attr(),
  DurationInMinutes: DS.attr(),
  StartTerminal: DS.attr(),
  StartGate: DS.attr(),
  EndTerminal: DS.attr(),
  EndGate: DS.attr(),
  PriceInCents: DS.attr("number"),
  CurrencyCode: DS.attr(),
  TransportationIdentifier: DS.attr(),
  EventType: DS.attr("number"),
  IsEndingEvent: DS.attr(),
  TripIdeas: DS.attr(),
  PartnerIdentifier: DS.attr(),
  Images: DS.attr(),
  FlightUpdateInformation: DS.attr(),

  getEventTypeDisplayName() {
    if (this.EventType == null) {
      return "Event";
    } else {
      return EventTypes[this.EventType].DisplayName;
    }
  },

  getEventTypeMaterialIcon() {
    if (this.EventType == null) {
      return "";
    } else {
      return EventTypes[this.EventType].DisplayName;
    }
  }
});
