import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  Id: DS.attr("number"),
  FullName: DS.attr("string"),
  ImageUrl: DS.attr("string"),
  ValidatedImageUrl: computed("ImageUrl", function() {
    if (this.ImageUrl.startsWith("/")) {
      return "https://devteam.travefy.com/" + this.ImageUrl;
    } else {
      return this.imageUrl;
    }
  }),
  Username: DS.attr("string"),
  IsAgent: DS.attr("boolean"),
  SubscriptionPeriodEnd: DS.attr("date"),
  AgentSubscriptionIsActive: DS.attr("boolean"),
  Title: DS.attr("string"),
  Phone: DS.attr("string"),
  Url: DS.attr("string"),
  CompanyLogoUrl: DS.attr("string")
});
