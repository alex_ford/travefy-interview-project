import DS from 'ember-data';

export default DS.Model.extend({
  Id: DS.attr("number"),
  TripId: DS.attr("number"),
  Title: DS.attr("string"),
  Date: DS.attr("date"),
  Ordinal: DS.attr(),
  IsActive: DS.attr("boolean"),
  CreatedOn: DS.attr("date"),

  TripEvents: DS.hasMany("trip-event"),

  PartnerIdentifier: DS.attr(),
  IsSupplemental: DS.attr("boolean")

});
