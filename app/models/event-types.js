/**
 * This module represents the various types of Trip Events supported by the
 * Travefy Platform. It also includes some app-specific properties which are
 * local to this app (i.e. not shared with the server).
 */

/**
 * See: https://developer.travefy.com/#18a8408b-9aaa-4d06-a275-828bfac1f58a
 * for documentation on the Event Types.
 *
 * See: https://material.io/tools/icons/?style=baseline
 * for a catalog of Material Icons that can be used here.
 */

// ["Flight", 0
//   Car Rental 1
//   Train 2
//   Cruise 3
//   Bus 4
//   Walk 5
//   Hotel 6
//   Vacation Rental 7
//   Camp 8
//   Event 9
//   Automobile 10
//   Food & Drink 11
//   Info 12
// ]
const EventTypes = [];
// This first one cooresponds to the 'null' value the API can return for EventType.
EventTypes[-1] = {
  TypeName: "Event",
  DisplayName: "Event",
  MaterialIcon: "event"
};
EventTypes[0] = {
  TypeName: "Flight",
  DisplayName: "Flight",
  MaterialIcon: "flight"
};
EventTypes[1] = {
  TypeName: "Car Rental",
  DisplayName: "Transporation -> Car Rental",
  MaterialIcon: "directions_car"
};
EventTypes[2] = {
  TypeName: "Train",
  DisplayName: "Transportation -> Rail",
  MaterialIcon: "train"
};
EventTypes[3] = {
  TypeName: "Cruise",
  DisplayName: "Cruise",
  MaterialIcon: "directions_boat"
};
EventTypes[4] = {
  TypeName: "Bus",
  DisplayName: "Transportation -> Other",
  MaterialIcon: "directions_bus"
};
EventTypes[5] = {
  TypeName: "Walk",
  DisplayName: "N/A",
  MaterialIcon: "directions_walk"
};
EventTypes[6] = {
  TypeName: "Hotel",
  DisplayName: "Lodging",
  MaterialIcon: "hotel"
};
EventTypes[7] = {
  TypeName: "Vacation Rental",
  DisplayName: "N/A",
  MaterialIcon: "home"
};
EventTypes[8] = {
  TypeName: "Camp",
  DisplayName: "N/A",
  MaterialIcon: ""
};
EventTypes[9] = {
  TypeName: "Event",
  DisplayName: "Activity",
  MaterialIcon: "local_activity"
};
EventTypes[10] = {
  TypeName: "Automobile",
  DisplayName: "N/A",
  MaterialIcon: "directions_car"
};
EventTypes[11] = {
  TypeName: "Food/Drink",
  DisplayName: "Food/Drink",
  MaterialIcon: "fastfood"
};
EventTypes[12] = {
  TypeName: "Info",
  DisplayName: "Info",
  MaterialIcon: "perm_device_information"
};
export default EventTypes;
