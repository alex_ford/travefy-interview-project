import DS from 'ember-data';
import { computed } from '@ember/object';

// A 'trip' represents multiple 'trip-days'.
export default DS.Model.extend({
  Id: DS.attr("number"),
  VerificationKey: DS.attr(),
  ShareUrlPath: DS.attr("string"),
  ValidatedShareUrlPath: computed("ShareUrlPath", function () {
    if (this.ShareUrlPath.startsWith("/")) {
      return "https://devteam.travefy.com/" + this.ShareUrlPath;
    } else {
      return this.ShareUrlPath;
    }
  }),
  Name: DS.attr("string"),
  Active: DS.attr("boolean"),
  CreatedOn: DS.attr("date"),
  InviteMessage: DS.attr("string"),
  TripCoverPhotoUrl: DS.attr("string"),
  ValidatedTripCoverPhotoUrl: computed("TripCoverPhotoUrl", function () {
    if (this.TripCoverPhotoUrl.startsWith("/")) {
      return "https://devteam.travefy.com/" + this.TripCoverPhotoUrl;
    } else {
      return this.TripCoverPhotoUrl;
    }
  }),
  EstimatedCost: DS.attr("number"),
  IsCostPerPerson: DS.attr("boolean"),
  IsChatDisabled: DS.attr("boolean"),
  IsPdfEnabled: DS.attr("boolean"),
  IsAppEnabled: DS.attr("boolean"),

  TripDays: DS.hasMany("trip-day"),

  SecondaryLogoUrl: DS.attr("string"),
  PartnerIdentifier: DS.attr(),
  Status: DS.attr(),
  IsArchived: DS.attr("boolean"),

  // TripStartDate: computed("TripDays.[]", function(one) {
  //   return this.get('TripDays').map(function(tripDay, index) {
  //     return `TRIPDAY ${index}: ${tripDay}`;
  //   });
  // })

  // this sorta works, but not really :()
  // TripStartDate: computed("TripDays.[]", function () {
  //   let result = this.TripDays.mapBy("Date");
  //   // console.log(result[1]);
  //   // console.log(result[result.length - 1]);
  //   return result[1];
  // }).volatile(),

  // TripEndDate: computed("TripDays.[]", function() {
  //   let result = this.TripDays.mapBy("Date");
  //   return result[result.length - 1];
  // }).volatile(),

  // TripDuration: computed("TripDays.[]", function() {
  //   return "";
  // })

});
