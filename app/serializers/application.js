import DS from 'ember-data';

/**
 * Application-wide (shared) Serializer. The code here will generally be applied
 * to all the other data serialization operations, unless a specific Serializer
 * exists for that particular data.
 */
export default DS.JSONSerializer.extend({
  primaryKey: "Id"
});
