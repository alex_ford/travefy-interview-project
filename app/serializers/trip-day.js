import DS from 'ember-data';

export default DS.JSONSerializer.extend(DS.EmbeddedRecordsMixin, {
  primaryKey: "Id",
  attrs: {
    TripEvents: {
      embedded: "always"
    }
  }
});
