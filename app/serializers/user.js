import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({

  /**
   * Override interface function so that we treat the server response as having
   * a bunch of 1st level JSON properties that are considered "metadata", then
   * after extracting them, redefine the payload to be just the contents of the
   * "User" object nested within the original response.
   *
   * @param {*} store
   * @param {*} typeClass
   * @param {*} payload
   */
  extractMeta(store, typeClass, payload) {
    // extract all first level keys as meta
    let meta = {};
    meta.AccessToken = payload.AccessToken;
    delete payload.AccessToken;
    meta.PublicKey = payload.PublicKey;
    delete payload.PublicKey;
    meta.AgentSubscriptionLevel = payload.AgentSubscriptionLevel;
    delete payload.AgentSubscriptionLevel;
    meta.IsActive = payload.IsActive;
    delete payload.IsActive;
    meta.CreatedOn = payload.CreatedOn;
    delete payload.CreatedOn;

    // promote nested "User" object as the full payload by moving each of the
    // attributes up one, then deleting the old (now empty) User attribute.
    let userData = payload.User;
    Object.keys(userData).forEach(function (key) {
      payload[key] = userData[key];
    });
    delete payload.User;
    return meta;
  }

});
