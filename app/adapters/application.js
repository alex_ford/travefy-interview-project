import DS from 'ember-data';

import ENV from 'travefy-interview-project/config/environment';

export default DS.RESTAdapter.extend({
  host: "https://api.travefy.com",
  namespace: "api/v1",

  init() {
    this._super(...arguments);
    this.set("headers", {
      "X-API-PUBLIC-KEY": ENV.APP["X-API-PUBLIC-KEY"],
      "X-API-PRIVATE-KEY": ENV.APP["X-API-PRIVATE-KEY"],
      "X-USER-TOKEN": ENV.APP["X-USER-TOKEN"]
      // "Content-Type": "application/json"
    });
  }

});
