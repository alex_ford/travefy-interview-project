import Route from '@ember/routing/route';

export default Route.extend({
  beforeModel() {
    // declare that 'trips' is the starting point for our app
    this.replaceWith("trips");
  }
});
