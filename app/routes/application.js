import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    // hardcoded to my account (arf4188+dev@gmail.com) for the purposes of the
    // interview project
    return this.store.findRecord("user", 2219659);
  }

});
