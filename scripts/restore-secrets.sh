#!/bin/bash

###
# Restore the contents of the `./secrets.tgz.gpg` encrypted archive at the
# project root.
#
# See companion script: `save-secrets.sh`
#
# Author: Alex Richard Ford (arf4188@gmail.com)
###

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_ROOT="$(dirname ${SCRIPTS_DIR})"

# Avoid specifying these as absolute paths, else tar will create the files as such
ENCRYPTED_ARCHIVE="secrets.tgz.gpg"
ARCHIVE="${ENCRYPTED_ARCHIVE%.gpg}"
SECRETS_DIR="./secrets/"

CWD="$(pwd)"

cd ${PROJECT_ROOT} &&
  gpg --output ${ARCHIVE} --decrypt ${ENCRYPTED_ARCHIVE} &&
  tar xvf ${ARCHIVE}

cd ${CWD}
