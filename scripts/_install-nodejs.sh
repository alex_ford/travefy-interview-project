#!/bin/bash

###
# Installs NodeJS (along with NPM) from the official website.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
###

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

source "${SCRIPTS_DIR}/_common.sh"

NODEJS_BASEURL="https://nodejs.org/dist/v10.15.3/"
NODEJS_FILENAME="node-v10.15.3-linux-x64.tar.xz"
NODEJS_VERSION="10.15.3"
info "installing NodeJS v${NODEJS_VERSION}"

nodejs --version >/dev/null 2>&1
if [ $? == 0 ]; then
  error "existing NodeJS detected!"
  error "    $(nodejs --version)"
  error "please remove/uninstall existing version."
  exit 1
fi

which curl >/dev/null 2>&1
if [ $? != 0 ]; then
  error "curl is not intalled and is required!"
  error "please install curl then try again."
  exit 1
fi

if [ -e "${NODEJS_FILENAME}" ]; then
  info "download already present, skipping download step."
else
  curl -LO "${NODEJS_BASEURL}/${NODEJS_FILENAME}"
  assertSuccess "a problem occurred while downloading NodeJS!"
fi

tar xvf "${NODEJS_FILENAME}"
assertSuccess "could not extract NodeJS from tarball."

mv -v "node-v10.15.3-linux-x64/" ~/.local/lib/
assertSuccess "could not move extracted NodeJS files to '~/.local/lib/'."

ln -sv ~/.local/lib/node-v10.15.3-linux-x64 ~/.local/lib/nodejs
assertSuccess "could not create symbolic link."

echo "" >> ~/.profile
echo "export OLDPATH=${PATH}" >> ~/.profile
echo "export PATH=~/.local/lib/nodejs/bin:\${OLDPATH}" >> ~/.profile
info "refreshing current shell environment"
. ~/.profile

info "node --version"
node --version
info "npm --version"
npm --version
