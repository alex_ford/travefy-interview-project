#!/bin/bash

###
# Save the contents of the `./secrets` directory at the project root into an
# encrypted archive file which can be safely committed to version control.
#
# See companion script: `restore-secrets.sh`
#
# Author: Alex Richard Ford (arf4188@gmail.com)
###

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_ROOT="$(dirname ${SCRIPTS_DIR})"

# Avoid specifying these as absolute paths, else tar will create the files as such
ENCRYPTED_ARCHIVE="secrets.tgz.gpg"
ARCHIVE="${ENCRYPTED_ARCHIVE%.gpg}"
SECRETS_DIR="./secrets/"

CWD="$(pwd)"

cd ${PROJECT_ROOT} &&
  tar cvf ${ARCHIVE} ${SECRETS_DIR} &&
  gpg --output ${ENCRYPTED_ARCHIVE} --symmetric ${ARCHIVE}

cd ${CWD}
