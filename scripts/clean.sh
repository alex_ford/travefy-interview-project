#!/bin/bash

###
# Clean script.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
###

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

source "${SCRIPTS_DIR}/_common.sh"

WORKING_DIR="$(pwd)"
cd "${PROJECT_DIR}"

rm -rv ./secrets/
rm -v ./secrets.tgz
rm -rv ./dist/
rm -rv ./node_modules/
rm -rv ./.vscode/extensions/
rm -rv ./.vscode/user-data/

echo "Clean done!"
cd "${WORKING_DIR}"
