#!/bin/bash

source ./env-setup.sh

function get_echo() {
  ENDPOINT="${BASE_ENDPOINT_URL}api/v1/echo"
  curl --verbose \
    --location --request GET "${ENDPOINT}" \
    --header "X-API-PUBLIC-KEY: ${API_PUBLIC_KEY}"
}

get_echo
