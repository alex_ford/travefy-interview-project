#!/bin/bash

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_API_DIR="$(dirname ${THIS_SCRIPT})"
SCRIPTS_DIR="$(dirname ${SCRIPTS_API_DIR})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

source ${SCRIPTS_API_DIR}/env-setup.sh

function get_trip() {
  ENDPOINT="${BASE_ENDPOINT_URL}api/v1/trips/2170709"
  curl --verbose \
    --location --request GET "${ENDPOINT}" \
    --header "X-USER-TOKEN: ${API_USER_TOKEN}" \
    --header "X-API-PUBLIC-KEY: ${API_PUBLIC_KEY}"
}

get_trip
