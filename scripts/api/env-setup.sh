#!/bin/bash

# This is intended to be sourced by each script in order to setup common
# stuff needed by all the scripts in this same directory.

THIS_SCRIPT="$(realpath $BASH_SOURCE)"
# echo "[ DBUG ] THIS_SCRIPT = ${THIS_SCRIPT}"
API_SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
SCRIPTS_DIR="$(dirname ${API_SCRIPTS_DIR})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"
SECRETS_DIR="${PROJECT_DIR}/secrets/"

if [ ! -d "${SECRETS_DIR}" ]; then
  echo "[ ERRO ] 'secrets' directory not found at: ${SECRETS_DIR}"
  exit 1
fi

BASE_ENDPOINT_URL="https://api.travefy.com/"
API_PUBLIC_KEY="$(cat ${SECRETS_DIR}/api-public.key)"
API_PRIVATE_KEY="$(cat ${SECRETS_DIR}/api-private.key)"
API_USER_TOKEN="$(cat ${SECRETS_DIR}/api-user.token)"


