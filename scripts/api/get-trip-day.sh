#!/bin/bash

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_API_DIR="$(dirname ${THIS_SCRIPT})"
SCRIPTS_DIR="$(dirname ${SCRIPTS_API_DIR})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

source ${SCRIPTS_API_DIR}/env-setup.sh

# https://developer.travefy.com/#afbd8486-ae53-4570-bf88-c4ade807159e
function get_trip_days() {
  TRIP_ID="2170709"
  TRIP_DAY_ID="11023296"
  ENDPOINT="${BASE_ENDPOINT_URL}api/v1/tripDays/${TRIP_DAY_ID}"
  curl --verbose \
    --location --request GET "${ENDPOINT}" \
    --header "X-USER-TOKEN: ${API_USER_TOKEN}" \
    --header "X-API-PUBLIC-KEY: ${API_PUBLIC_KEY}" \
    --header "X-TRIP-ID: ${TRIP_ID}"
}

get_trip_days
