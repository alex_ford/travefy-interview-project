#!/bin/bash

source ./env-setup.sh

function api_secure_echo() {
  ENDPOINT="${BASE_ENDPOINT_URL}api/v1/secureEcho"
  curl --verbose \
    --location --request GET "${ENDPOINT}" \
    --header "X-API-PUBLIC-KEY: ${API_PUBLIC_KEY}" \
    --header "X-API-PRIVATE-KEY: ${API_PRIVATE_KEY}"
}

api_secure_echo
