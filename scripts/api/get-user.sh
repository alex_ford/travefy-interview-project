#!/bin/bash

source ./env-setup.sh

function get_user() {
  ENDPOINT="${BASE_ENDPOINT_URL}api/v1/users/2219659"
  curl --verbose \
    --location --request GET "${ENDPOINT}" \
    --header "X-USER-TOKEN: ${API_USER_TOKEN}" \
    --header "X-API-PUBLIC-KEY: ${API_PUBLIC_KEY}" \
    --header "X-API-PRIVATE-KEY: ${API_PRIVATE_KEY}"
}

get_user
