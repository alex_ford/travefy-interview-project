#!/bin/bash

# Runs Google Chrome, configured for development/testing purposes.
#
# Author: Alex Richard Ford (arf4188@gmail.com)

# CORS
# Communication between the ember app and the API server can get blocked due
# to a violation of the CORS Policy in the browser. This shouldn't be an issue
# if/when this app is hosted behind the travefy.com domain, but when hosted
# locally or on another temporary domain, we can just turn it off in the client
# browser.
#
# This solves the following error:
# Access to XMLHttpRequest at 'https://api.travefy.com//api/v1/trips' from
# origin 'http://localhost:4200' has been blocked by CORS policy: Response to
# preflight request doesn't pass access control check: No
# 'Access-Control-Allow-Origin' header is present on the requested resource.

google-chrome --disable-web-security \
              --user-data-dir="$(realpath ~/.local)/data/google-chrome" \
              http://localhost:4200
