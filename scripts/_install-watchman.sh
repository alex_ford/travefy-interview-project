#!/bin/bash

###
# https://facebook.github.io/watchman/docs/install.html
###

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

source "${SCRIPTS_DIR}/_common.sh"

which watchman >/dev/null 2>&1
if [ $? == 0 ]; then
  error "it appears 'watchman' is already installed."
  error "watchman v$(watchman --version)"
  error " \-- $(which watchman)"
  exit 1
fi

info "installing watchman from source... (this may take awhile)"

sudo apt install -y libssl-dev &&
  sudo apt install -y autoconf &&
  sudo apt install -y automake &&
  sudo apt install -y libtool

if [ $? != 0 ]; then
    error "there was a problem installing build environment prerequisites!"
    exit 1
fi

if [ ! -d "./watchman/" ]; then
  git clone --depth 1 https://github.com/facebook/watchman.git
fi

cd watchman &&
  git checkout v4.9.0 &&
  ./autogen.sh &&
  ./configure &&
  make &&
  sudo make install

if [ $? == 0 ]; then
  info "watchman install has completed successfully!"
else
  error "there was a problem installing watchman!"
fi
