#!/bin/bash

###
# Helper script to get the ember app served up quickly.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
###

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

source "${SCRIPTS_DIR}/_common.sh"

WORKING_DIR="$(pwd)"
cd "${PROJECT_DIR}"

info "If successful, app will be at http://localhost:4200/"
ember serve

# info "If successful, app will be at https://localhost:4200/"
# ember serve --ssl true

cd "${WORKING_DIR}"

