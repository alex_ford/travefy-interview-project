#!/bin/bash

###
# Author: Alex Richard Ford (arf4188@gmail.com)
###

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

source "${SCRIPTS_DIR}/_common.sh"

info "${0} will perform the necessary steps to create a development environment for this project."
read -p "Continue? [y/n] "
if [ "${REPLY}" == "n" ]; then
    warn "Aborting! Nothing has been done."
    exit 0
elif [ "${REPLY}" != "y" ]; then
    error "Please answer with 'y' or 'n'."
    exit 1
fi

sudo npm install -g ember-cli
assertSuccess "a problem occurred while installing 'ember-cli'."

info "if you'd like to install 'watchman', execute the ./_install-watchman.sh script."
