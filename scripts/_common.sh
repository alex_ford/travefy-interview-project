###
# Common Bash script intended to be sourced by other scripts.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
###

function debug() {
  MSG="${1}"
  C="\e[33m"
  R="\e[0m"
  if [ ! -z ${DEBUG} ]; then
    echo -e "[ ${C}DBUG${R} ] ${MSG}"
  fi
}

function info() {
  MSG="${1}"
  echo -e "[ INFO ] ${MSG}"
}

function warn() {
  MSG="${1}"
  C="\e[32m"
  R="\e[0m"
  echo -e "[ ${C}WARN${R} ] ${MSG}"
}

function error() {
  MSG="${1}"
  C="\e[31m"
  R="\e[0m"
  echo -e "[ ${C}ERRO${R} ] ${MSG}"
}

debug "testing DEBUG"
# info "testing INFO"
# warn "testing WARN"
# error "testing ERROR"

function assertSuccess() {
  if [ $? != 0 ]; then
    MSG_ON_ERROR="${1}"
    error "${MSG_ON_ERROR}"
    exit 1
  fi
}
