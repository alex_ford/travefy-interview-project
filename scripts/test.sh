#!/bin/bash

###
# Captures the test comamnd/environment for the project.
#
# Author: Alex Richard Ford (arf4188@gmail.com)
###

THIS_SCRIPT="$(realpath $0)"
SCRIPTS_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPTS_DIR})"

source "${SCRIPTS_DIR}/_common.sh"

WORKING_DIR="$(pwd)"
cd "${PROJECT_DIR}"

ember test --environment "development"

cd "${WORKING_DIR}"
