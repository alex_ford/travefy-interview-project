import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | get-event-type-display-name', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    this.set('inputValue', '-1');

    await render(hbs`{{get-event-type-display-name inputValue}}`);

    assert.equal(this.element.textContent.trim(), 'Event');
  });

  test('it renders', async function (assert) {
    this.set('inputValue', '6');

    await render(hbs `{{get-event-type-display-name inputValue}}`);

    assert.equal(this.element.textContent.trim(), 'Lodging');
  });
});
