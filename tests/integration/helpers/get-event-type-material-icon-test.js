import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | get-event-type-material-icon', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    this.set('inputValue', '-1');

    await render(hbs`{{get-event-type-material-icon inputValue}}`);

    assert.equal(this.element.textContent.trim(), 'event');
  });

  test('it renders', async function (assert) {
    this.set('inputValue', '6');

    await render(hbs `{{get-event-type-material-icon inputValue}}`);

    assert.equal(this.element.textContent.trim(), 'hotel');
  });
});
