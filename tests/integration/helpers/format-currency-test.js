import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Helper | format-currency', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    this.set('inputValue', '1234');

    await render(hbs`{{format-currency inputValue}}`);

    assert.equal(this.element.textContent.trim(), '$1234.00');
  });

  test('it renders', async function (assert) {
    this.set('inputValue', '2.5');

    await render(hbs `{{format-currency inputValue}}`);

    assert.equal(this.element.textContent.trim(), '$2.50');
  });

  test('it renders', async function (assert) {
    this.set('inputValue', '1234.1234');

    await render(hbs `{{format-currency inputValue}}`);

    assert.equal(this.element.textContent.trim(), '$1234.12');
  });

  test('it renders', async function (assert) {
    this.set('inputValue', '1234.5678');

    await render(hbs `{{format-currency inputValue}}`);

    assert.equal(this.element.textContent.trim(), '$1234.57');
  });

  test('it renders', async function (assert) {
    this.set('inputValue', '.25');

    await render(hbs `{{format-currency inputValue}}`);

    assert.equal(this.element.textContent.trim(), '$0.25');
  });
});
