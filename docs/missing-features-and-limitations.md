Missing Features and Limitations
================================

Given my ability to make enough time to work on this project, and the fact that I was learning EmberJS for the first time (as well as refreshing my JavaScript, and CSS) there are a few things I would have done either differently, or additionally under different circumstances:

* Ember Components
    * I didn't create any Ember Components, but I would have given more time. These would have simply been reusable versions of various pieces of the view ("templates").

* Testing Coverage and 100% Pass
    * There are only a couple of tests that I actually added to the code base myself... the rest are from ember-cli's generation process. Given more time, I would have liked to flush out meaningful tests for most of the code.
    * Currently, of the tests that exist (about 78 of them), 2 of them are failing.
        * The failures don't stop the app from working... they are "danger" warnings from TemplateLint regarding the use of "triple-curlies" in the template in order to display embedded HTML received from the API.

* Deployment
    * I only ever got around to getting things running in "development" mode...

* Better Responsive Design/Layout
    * Especially for the "Trip Details" page on mobile/smaller resolution devices.

* Switch to using a proxy to solve CORS problem instead of using a "web-security" disabled Chrome.

* Show a nice error if/when a problem occurs (e.g. no Internet)

* As well as the TODOs inlined throughout the code... (about 8 in `application.hbs`, `trip-details.hbs`, and `index.html`)
