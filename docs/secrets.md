Secrets
=======

The `../secrets.tgz.gpg` tarball contains sensitive information that must have limited user access. A separate password is needed to extract the files to a `../secrets/` directory. This directory, the unencrypted tarball, and any other sensitive files should be listed in the `../.gitignore` file, thus should not get checked into Git.

# Creating/Updating the Secrets Tarball

Leverage the included `../scripts/save-secrets.sh` script to take the contents of the `../secrets/` directory, tar it, then encrypted it. The encrypted archive can now be committed to version control and pushed upstream.

# Extracting the Secrets Tarball

Leverage the included `../scripts/restore-secrets.sh` script to take the encrypted archive at `../secrets.tgz.pgp`, decrypt it, and untar it. The `../secrets/` directory should now be available/updated with the contents of the encrypted archive.

# Bottom Line

* NEVER store secret/sensitive information in plain-text that will be committed to Git/version control.
* Ensure that `../.gitignore` is updated to exclude any new secrets that may be introduced in the future.
