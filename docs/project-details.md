Project Details
===============

Develop a mobile friendly Ember application that connects to a specific account in Travefy using the Travefy API.

The application should:

1. Load a list of the user's trips
2. Allow the user to navigate to a day on the trip
3. Render the events in the trip with the following information:
            - Name/title of event
            - Event description/notes as rendered html (for example links should be clickable and strong tags should be bold; only a subset of html is required, for more information you should reference the text editor on travefy.com)
			- Images or places
4. The user should be able to then navigate between trips and days.
5. The back button in the browser should work to navigate within the application.
6. The application should perform reasonably well with 30 or fewer trips, 30 or fewer days per trip, and 30 or fewer events per day.

Optional:

You should reference https://www.emberjs.com/ for guidelines on application architecture.

Feel free to incorporate add-ons from https://www.emberaddons.com/ or https://www.emberobserver.com/ if you find something that is appropriate, but be prepared to explain HOW you might implement the add-on yourself if you use one.

You should reference the trip plans or our itinerary app on the google play store for guidance on style or interactions. You can make it as nice as you want in a reasonable period of time.

If you are getting done with everything quickly feel free to add some editing capabilities.

# Links

* Travefy API Endpoint:
    * https://api.travefy.com

* Travefy API Documentation:
    * https://developer.travefy.com

* Travefy Trips Webapp:
    * https://devteam.travefy.com/
