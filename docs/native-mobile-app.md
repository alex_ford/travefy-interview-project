Native Mobile App
=================

Consider looking into http://corber.io/, which:

> is a CLI that compiles Ember, Vue, React Apps/PWA’s into native mobile applications, while providing developer friendly features such as hot reload on devices + emulators.
