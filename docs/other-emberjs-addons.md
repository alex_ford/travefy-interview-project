Other EmberJS Addons
====================

This is a semi-curated list of EmberJS Addons that might be interesting for this project. Most (if not all) of the addons can be found on [Ember Observer](https://emberobserver.com/).

# EmberCLI Mirage

https://www.ember-cli-mirage.com/

> A client-side server to help you build, test and demo your Ember app.

# LiquidFire

https://ember-animation.github.io/liquid-fire/

> A toolkit for managing animated transitions in an Ember application.

# FastBoot

http://www.ember-fastboot.com/

> Progressive enhancement for ambitious web apps.
